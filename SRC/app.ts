import express from "express"
import {ControllerEntidad} from "./routes/entidad.controller"
import logger from "morgan"


export default class App {
  public app: express.Application
  public rutas: ControllerEntidad = new ControllerEntidad()
  constructor() {
    this.app= express()
    this.app.use(logger('dev'))
    this.app.use(express.json())
    this.rutas.routes(this.app)
    // this.rutas.pruebas(this.app)
    // this.user.pruebas(this.app)
  }
}

 /*
app.get('/', (req, res) => {
  res.send('Hello World')
})
 */
//