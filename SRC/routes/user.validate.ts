import joi from "joi";

export const schemaGetDelete =joi.object({
    id:joi.number().integer()
})

export const schemaPostPut = joi.object({
    id:joi.number().integer(),
    nombre: joi.string().min(3).max(30)
})