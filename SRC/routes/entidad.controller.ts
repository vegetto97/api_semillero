import express from "express"
import { schemaGetDelete, schemaPostPut } from "./user.validate";
import { UserService } from "../services/user.service";



export class ControllerEntidad {

    public routes(app: any) {

        app.route('/user').get(async (req: express.Request, res: express.Response) => {
            console.log(req.query)
            const validation = schemaGetDelete.validate(req.query)
            if (validation.error) {
                res.status(422).json({
                    data: null,
                    status: validation.error.message
                })
                res.end()
            } else {
                const objUserService: UserService = new UserService()
                const objElement = await objUserService.getUser(Number(req.query.id))
                if (objElement === false) {
                    res.status(200).json({
                        data: objElement,
                        status: "Usuario no encontrado"
                    })
                    res.end()
                }else{
                    res.status(200).json({
                        data: objElement
                    })
                    res.end()
                }
            }
        })

        app.route('/user/lista').get(async (req: express.Request, res: express.Response) => {
            console.log(req.query)
            const validation = schemaGetDelete.validate(req.query)
            if (validation.error) {
                res.status(422).json({
                    data: null,
                    status: validation.error.message
                })
                res.end()
            } else {
                const objUserServiceList: UserService = new UserService()
                const objElementList = await objUserServiceList.getUserList()
                res.status(200).json({
                    data: objElementList,
                    status: null
                }).end()
            }

        })

        // Metodo post agregando nuevo campo


        app.route('/user').post((req: express.Request, res: express.Response) => {
            console.log(req.body)
            const validation = schemaPostPut.validate(req.body)
            if (validation.error) {
                res.status(422).json({
                    data: null,
                    status: validation.error.message
                })
                res.end()
            } else {

                const objUserServicePost: UserService = new UserService()
                const objElement = objUserServicePost.postUser(req.body.nombre)
                res.status(201).json({
                    data: objElement,
                    status: null
                })
                res.end()
            }

        })

        app.route('/user').put(async (req: express.Request, res: express.Response) => {
            const validation = schemaPostPut.validate(req.body)
            if (validation.error) {
                res.status(422).json({
                    data: null,
                    status: validation.error.message
                })
                res.end()
            } else {
                const objUserServicePut: UserService = new UserService()
                const objElement = await objUserServicePut.putUser(req.body.id, req.body.nombre)
                if (objElement === false) {
                    res.status(200).json({
                        data: objElement,
                        status: "Usuario no encontrado"
                    })
                    res.end()
                } else {
                    res.status(200).json({
                        data: objElement,
                        status: "Usuario editado"
                    })
                    res.end()
                }
            }
        })



        app.route('/user').delete(async (req: express.Request, res: express.Response) => {
            const validation = schemaGetDelete.validate(req.body)
            if (validation.error) {
                res.status(422).json({
                    data: null,
                    status: validation.error.message
                })
                res.end()
            } else {

                const objUserServiceDelete: UserService = new UserService()
                const objElement = await objUserServiceDelete.deleteUser(req.body.id)


                if (objElement === false) {
                    // res.send(`el Id no esta en la base de datos`)
                    res.status(200).json({
                        data: {},
                        status: "El Id no existe en la base de datos"
                    })
                    res.end()
                } else {

                    // res.send("Usuario eliminado")
                    res.status(200).json({
                        data: objElement,
                        status: "Usuario eliminado"
                    }).end()
                }


            }
        })
    }

}







