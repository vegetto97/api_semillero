import { Config } from "../config/config";
import { Client } from "pg";

export class UserService {
    private id: number
    private name: string
    private status: boolean
    constructor() {

        this.id = -1
        this.name = ""
        this.status = false
    }
    public getUser = async (id: number): Promise<any> => {
        const objConfig: Config = new Config()

        const client: Client = new Client({ connectionString: objConfig.connection() })

        client.connect((err) => {
            console.log(err)
            if (err) {
                throw new Error('Internal Server Error')
            }
        })

        try {
            let validar=true
            const result = await client.query(`SELECT * FROM users WHERE id = '${id}' AND status = 'true'`)
            if (result.rowCount === 1) {
                this.id = result.rows[0].id,
                    this.name = result.rows[0].nombre,
                    this.status = result.rows[0].status

                return {
                    id: this.id,
                    name: this.name,
                    status: this.status
                }
            } else {
                console.log(`El numero Id ${id} no esta en la base datos`)
                return validar=false
            }

            // console.log(result.rows[0])
        } catch (error) {
            throw new Error('Internal Server Error')
        }

        /*
                if (id<0){
                    throw new Error('Internal Server Error')
                }
                    ListUser.forEach(element => {
                        if(element.id === id && element.status){
                            this.id=element.id
                            this.name=element.nombre


                        }

                    })
                    return {id:this.id, name:this.name}
                    */
    }

    public getUserList = async (): Promise<any> => {
        const objConfig: Config = new Config()

        const client: Client = new Client({ connectionString: objConfig.connection() })

        client.connect((err) => {
            console.log(err)
            if (err) {
                throw new Error('Internal Server Error')
            }
        })
        try {
            const result = await client.query(`SELECT * FROM users WHERE status = 'true' ORDER BY id`)
            const all = result.rows
            return all


        } catch (error) {
            throw new Error('Internal server Error')
        }
    }

    public postUser = async (name: string): Promise<any> => {

        const objConfig: Config = new Config()
        const client: Client = new Client({ connectionString: objConfig.connection() })

        client.connect((err) => {
            console.log(err)
            if (err) {
                throw new Error('Internal Server Error')
            }
        })

        try {
            const result = await client.query(`INSERT INTO users (nombre) VALUES('${name}')`)
            console.log("Usuario Ingresado correctamente")

        } catch (error) {
            throw new Error('Internal Server Error')
        }

    }


    public putUser = async (idAux: number, name: string): Promise<any> => {
        const objConfig: Config = new Config()

        const client: Client = new Client({ connectionString: objConfig.connection() })

        client.connect((err) => {
            console.log(err)
            if (err) {
                throw new Error('Internal Server Error')
            }
        })
        let validar = true
        try {
            const probar= await client.query(`select * from users WHERE id=${idAux} AND status = 'true'`)
            if(probar.rowCount===1){
                const result = await client.query(`UPDATE users SET nombre='${name}' WHERE id='${idAux}' AND status = 'true'`)
                if(result.rowCount===1){
                    console.log('Usuario Actualizado Correctamente')
                    validar=true
                    return validar
                }else{
                    validar=false
                    return validar
                }
            }else{
                validar = false
                return validar
            }
        } catch (error) {
            throw new Error('Internal Server Error')
        }

    }


    public deleteUser = async (idAux: number): Promise<any> => {
        const objConfig: Config = new Config()
        const client: Client = new Client({ connectionString: objConfig.connection() })

        client.connect((err) => {
            console.log(err)
            if (err) {
                throw new Error('Internal Server Error')
            }
        })
        let validar = true
        try {
            const probar= await client.query(`select * from users WHERE id=${idAux}`)

            if (probar.rowCount===1) {
                const result = await client.query(`UPDATE users SET status='false' WHERE id='${idAux}' AND status='true'`)
                return validar
            } else {
                validar = false
                return validar
            }
        } catch (error) {
            throw new Error('Internal Server Error')

        }

    }

}
